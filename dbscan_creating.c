/*!
 * file: sbscan_creating.h
 * create_db function file
 */

#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
#include <string.h>

typedef struct {
    int id;
    char manufacturer[127];
    int year;
    char model[127];
    float price;
    int x_size;
    int y_size;
} SCAN_INFO;


void create_db(const char *csv, const char *db) {
    FILE *fcsv, *fdb;
    char str[255], *fstr, tmpstr[255], dbstr[7][255];
    int n, i, j, k;
    unsigned char flag = 1;
    SCAN_INFO scn[100];

    fcsv = fopen(csv, "r");
    fdb = fopen(db, "wb");

    j = 0;
    while (!feof(fcsv)) {
        fscanf(fcsv, "%s", &str);

        strcpy(tmpstr, str);

        fstr = strstr(tmpstr, ";");
        n = fstr - tmpstr;
        strncpy(dbstr[0], &tmpstr[0], n);
        dbstr[0][n] = 0;
        strncpy(fstr, &fstr[1], strlen(fstr));

        for (i = 1; i < 7; i++) {

            strcpy(tmpstr, fstr);
            fstr = strstr(tmpstr, ";");
            if (fstr) {
                n = fstr - tmpstr;
                strncpy(dbstr[i], &tmpstr[0], n);
                dbstr[i][n] = 0;

                strncpy(fstr, &fstr[1], strlen(fstr));
            } else {
                strcpy(dbstr[i], tmpstr);
            }
        }

        strcpy(scn[j].manufacturer, dbstr[0]);
        strcpy(scn[j].model, dbstr[1]);
        scn[j].year = atoi(dbstr[2]);
        scn[j].price = atof(dbstr[3]);
        scn[j].x_size = atoi(dbstr[4]);
        scn[j].y_size = atoi(dbstr[5]);
        scn[j].id = atoi(dbstr[6]);


        j = j + 1;
    }

    while (flag) {
        flag = 0;
        for (i = 0; i < j; i++) {
            for (k = i + 1; k < j; k++) {
                if (scn[i].id == scn[k].id) {
                    scn[k].id = scn[k].id + 1;
                    flag = 1;
                }
            }
        }
    }

    fwrite(&j, sizeof(int), 1, fdb);
    for (i = 0; i < j; i++) {
        fwrite(&scn[i], sizeof(SCAN_INFO), 1, fdb);
    }


    fclose(fcsv);
    fclose(fdb);
}

int main() {
    FILE *fr;
    int test, i, j;
    SCAN_INFO scn[100];
    create_db("scanners.csv", "db");


    fr = fopen("db", "rb");
    fread(&test, sizeof(int), 1, fr);
    printf("\nsize of file: %d", test);

    for (i = 0; i < test; i++) {
        fread(&scn[i], sizeof(SCAN_INFO), 1, fr);
    }

    printf("\n\nData from binary file:\n");
    for (j = 0; j < test; j++) {
        printf("\n");
        printf("\nmanufacturer=%s", scn[j].manufacturer);
        printf("\nmodel=%s", scn[j].model);
        printf("\nyear=%d", scn[j].year);
        printf("\nprice=%f", scn[j].price);
        printf("\nx_size=%d", scn[j].x_size);
        printf("\ny_size=%d", scn[j].y_size);
        printf("\nid=%d", scn[j].id);
    }

    fclose(fr);


    getchar();
    return 0;
}
